#[allow(dead_code)]
#[derive(Debug, Clone)]
pub struct Person {
    name: String,
    age: i32,
}

#[derive(Debug, Clone, Copy)]
pub struct Point {
    x: i32,
    y: i32,
}

impl Point {
    pub fn new(x: i32, y: i32) -> Self {
        Point { x, y }
    }
}

fn main() {
    let x = 35;
    let y = x + 5;
    let mut p = Person {
        name: "Adriano".to_string(),
        age: 56,
    };
    let p2 = p.clone();
    p.name = "Adriano Vieira".to_string();
    println!("y = {}, y = {}", x, y);
    println!("p = {:?}, p2 = {:?}", p, p2);

    let mut pnt = Point::new(250, 300);
    let pnt2 = pnt;
    pnt.x += 30;
    pnt.y += 20;
    println!("pnt = {:?}, pnt2 = {:?}", pnt, pnt2);
}

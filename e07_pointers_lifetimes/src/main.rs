#[derive(Debug, Clone)]
pub struct Person {
    name: String,
    age: i32,
}

impl Person {
    pub fn new(name: String, age: i32) -> Self {
        Person { name, age }
    }

    pub fn greet(&self) -> String {
        format!("Hi! I am {} and I am {} years old", self.name, self.age)
    }
}
fn main() {
    let p = Person::new("Reserva".to_string(), 35);
    println!("Person = {:?}", p);

    let g1 = p.greet();
    println!("Greetings... {}", g1);
    let g2 = p.greet();
    println!("really... {}", g2);
}

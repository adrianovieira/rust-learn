use std::io;
use rand::Rng;
use std::cmp::Ordering;

/// The constant that define the maximum guessing range
const MAX_GUESS_RANGE: u32 = 100;

/// The Guessing Game routine
/// 
/// # The Guessing Game
/// It defines a secret number which the player needs to guess
fn guessing_game() {
    let secret_number = rand::thread_rng().gen_range(1..=MAX_GUESS_RANGE);

    println!("Guess the interger number from 1 to {MAX_GUESS_RANGE}! (0 = exit)");

    loop {
        println!("Please, input your guess' number:");

        let mut guess = String::new();

        io::stdin()
            .read_line(&mut guess)
            .expect("Failed to read line");
        
        let guess:u32 = match guess.trim().parse() {
            Ok(num) => num,
            Err(_e) => continue,
        };

        if guess == 0 {
            break;
        }
    
        print!("You guessed: {guess} ");
    
        match guess.cmp(&secret_number) {
            Ordering::Less=>println!("==> Too small"),
            Ordering::Equal=>{
                println!("=> You win! <=");
                break;
            },
            Ordering::Greater=>println!("==> Too big!"),
        }
        println!("");
    }    
}

fn main() {
    guessing_game();
}

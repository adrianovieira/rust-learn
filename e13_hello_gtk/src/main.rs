use gtk4 as gtk;
use gtk::prelude::*;

fn main() {
    let app = gtk::Application::builder()
        .application_id("io.vieira.HelloGTK")
        .build();

    app.connect_activate(|app| {
        let window = gtk::ApplicationWindow::builder().build();
        
        window.set_application(Some(app));
        window.set_title(Some("Hello, gtk-rs!"));
        window.set_default_width(150);
        window.set_default_height(70);

        let button = gtk::Button::with_label("Hello, world! Click here!");

        button.add_css_class("suggested-action");
        button.set_margin_top(15);
        button.set_margin_bottom(15);
        button.set_margin_start(15);
        button.set_margin_end(15);

        button.connect_clicked(|_| {
            eprintln!("Clicked!");
        });

        window.set_child(Some(&button));

        window.show();
    });

    app.run();
}
// Generics
fn divide(a: i32, b: i32) -> Result<i32, String> {
    if b == 0 {
        return Result::Err("Cannot divide by zero".to_string());
    }
    Result::Ok(a / b)
}

fn main() {
    let a = divide(15, 5);
    let b = divide(10, 0);
    match a {
        Result::Ok(v) => println!("value = {}", v),
        _ => {}
    }
    println!("a = {:?}, b = {:?}", a, b);
}

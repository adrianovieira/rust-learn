pub fn buble_sort<T: PartialOrd>(v: &mut [T]) {
    (0..v.len()).for_each(|q| {
        let mut sorted: bool = true;
        for i in 0..(v.len() - 1 - q) {
            if v[i] > v[i + 1] {
                v.swap(i, i + 1);
                sorted = false;
            }
        }
        if sorted {
            return;
        }
    });
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_bubble_sort() {
        let mut v = vec![4, 6, 1, 8, 11, 13, 3];
        buble_sort(&mut v);
        assert_eq!(v.len(), 7);
        assert_eq!(v, vec![1, 3, 4, 6, 8, 11, 13]);
        assert_ne!(v, vec![4, 6, 1, 8, 11, 13, 3]);
    }
}

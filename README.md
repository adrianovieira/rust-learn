# Rust lang

## Requirements

- Rust [^rust] programming language

### Rust Installation

- Rustup[^rustup] Rust toolchain installer; or
- Linux package manager; or
    - debian*:
     ```bash
     sudo apt-get install rustc cargo rust-clippy rust-all
     ```
    - Rocky Linux/Alma Linux/Fedora:
    ```bash
    sudo dnf install rust cargo
    ```
- Conda[^conda] package manager
  ```bash
  conda create -yn rust-lang
  conda activate rust-lang
  conda install gcc rust_linux-64 rust=1.61.0
  #conda deactivate
  ```

- GTK devel

  gtk-rs[^gtk-rs] that is a Rust bindings of GTK.

  - Alpine (3.16)

    ```bash
    sudo apk add rust cargo gtk4.0-dev
    ```

  - Ubuntu

    ```bash
    sudo apt-get install libgtk-4-dev
    ```

  - Rocky Linux/Alma Linux/Fedora:

    ```bash
    sudo dnf install gtk4-devel
    ```

## References

[^rustup]: [Rustup toolchain installer](https://www.rust-lang.org/tools/install/)
[^rust]: [Rust programming language](https://www.rust-lang.org)
[^conda]: [Conda package manager](https://docs.conda.io/projects/conda/en/latest/user-guide/)
[^gtk-rs]: [GTK-RS Rust bindings of GTK](https://gtk-rs.org/)

#[allow(dead_code)]
#[derive(Debug)]
pub struct LinkedList<T> {
    data: T,
    next: Option<Box<LinkedList<T>>>,
}

impl<T: std::ops::AddAssign> LinkedList<T> {
    pub fn add_up(&mut self, n: T) {
        self.data += n;
    }
}

fn main() {
    let mut ll = LinkedList {
        data: 3,
        next: Some(Box::new(LinkedList {
            data: 2,
            next: None,
        })),
    };
    if let Some(ref mut v) = ll.next {
        v.add_up(10);
    }
    println!("Hello, world! {:?}", ll);

    let s = "  hello  ";
    let s1 = s.trim();
    let p = s.to_string();

    println!("s = '{:?}' [{}]", s, s1);
    println!("t = {}",type_of(s));
    println!("t = {}",type_of(p));
    println!("t = {}",type_of(1.0));
}

fn type_of<T>(_: T) -> String {
    format!("{}",std::any::type_name::<T>())
}
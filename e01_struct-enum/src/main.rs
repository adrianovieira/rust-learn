// #[derive(Debug)]
pub enum Color {
    Red(String),
    Green,
    Blue,
}

// #[derive(Debug)]
pub struct Person {
    name:String,
    age: i32,
    children: i32,
    fave_color: Color,
}

impl Person {
    pub fn printf(self) -> String {
        format!("name = {}, age = {} has {} children, favorite color {}", 
            self.name, 
            self.age, 
            self.children,
            match self.fave_color {
                Color::Red(_) => "is red",
                Color::Green => "is green",
                Color::Blue => "is blue",
            }        
        )
    }
}

fn main() {
    let p = Person{
        name: "Adriano".to_string(),
        age: 56,
        children: 0,
        fave_color: Color::Blue,
    };
    let c = Color::Red("hi".to_string());
    match c {
        Color::Red(s) => println!("It's red {}", s),
        Color::Green => println!("It's green"),
        Color::Blue => println!("It's blue"),
    }

    println!("Hello, rust! This is: {}", p.printf());
}
